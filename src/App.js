import React, { Fragment } from 'react';

import './assets/styles/style.css';

import Routes from './routes';

function App() {
  return (
    <Fragment>
      <Routes />
    </Fragment>
  );
}

export default App;
