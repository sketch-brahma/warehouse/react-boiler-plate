# React Boilerplate

React app with routes configuration.

## Development Setup

- Clone Repository: `git clone HTTPS/SSH Url`
- Move to root directory: `cd react-boilerplate`
- Install dependency: `npm install`
- Starting Project: `npm start`
- Open link in browser: [http://localhost:3000/](http://localhost:3000/)

## Folder Structure

    react-boilerplate
    │
    └───src
    │   │
    │   └───assets (All static assets for the project)
    │   │
    │   └───conatiners (Major react components)
    │   │
    │   └───components (Reusable react child components)
    │   │
    │   └───routes (React routes configurations)
    │
    └───App.js (React root component)


## Developer Best Practice

- Maintain proper namespacing for folders, files, variable and function declarations.
- Format code using [Prettier](https://www.npmjs.com/package/prettier) package.
- Always create feature or bug branches and then merge with stable master branch.
- Provide proper commit messages & split commits meaningfully.
